package de.benediktstuhrmann.drawingtests;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.widget.CheckedTextView;

/**
 * Created by Max on 07.11.2016.
 * TODO: Zählen der Level aus DB-Methode und nicht mehr aus Anzahl der Objekte! Dann Objekte löschen!
 */

public class LevelDataProvider {

    LevelSelectActivity lsa = new LevelSelectActivity();
    UserDataSource uds = new UserDataSource(lsa.ctx);

    /**
     * Der Konstruktor dieser Klasse erstellt ebenfalls die Level, die später vorhanden sein sollen
     * SPÄTER: Falls Level Vorhanden, Level aus der Datenbank nehmen!
     */
    public LevelDataProvider() {
    }

    /**
     * Funktion befüllt die HashMap mit Key und Value Paaren
     * @return die fertige HashMap
     */
    public HashMap<String, List<String>> getInfo() {

        HashMap<String, List<String>> ThemaDetails = new HashMap<String, List<String>>();

        List<String> Geraden = new ArrayList<String>();
        List<String> Parabeln = new ArrayList<String>();
        List<String> Trigonometrische = new ArrayList<String>();

        uds.open();

        for(int addCounter = 0; addCounter <= 2; addCounter++) {
            Geraden.add(uds.getNameOfTheme(0) + " " + uds.getNameOfLevel(0, addCounter));
            Parabeln.add(uds.getNameOfTheme(1) + " " + uds.getNameOfLevel(1, addCounter));
            Trigonometrische.add(uds.getNameOfTheme(2) + " " + uds.getNameOfLevel(2, addCounter));
        }


        ThemaDetails.put(uds.getNameOfTheme(0), Geraden);
        ThemaDetails.put(uds.getNameOfTheme(1), Parabeln);
        ThemaDetails.put(uds.getNameOfTheme(2), Trigonometrische);

        uds.close();

        return ThemaDetails;
    }

    /**
     * Funktion fragt die Datenbank, ob das Level vor dem jetzigen Level schon abgeschlossen ist
     * @param groupPosition
     * @param childPosition
     * @return Ist das vorherige Level abgeschlossen, so wird ein true returnt
     */
    public boolean fetchFormerLevelCompleted(int groupPosition, int childPosition) {

        boolean databaseFormerLevelResult = false;
        int formerChildPosition = childPosition - 1;

        if(formerChildPosition <= 0) {
            formerChildPosition = 0;
        }

        uds.open();
            databaseFormerLevelResult = uds.getCompletedOfLevel(groupPosition, formerChildPosition);
        uds.close();

        return databaseFormerLevelResult;
    }

    /**
     * Funktion fragt die Datenbank, ob dieses Level schon abgeschlossen ist
     * @param groupPosition
     * @param childPosition
     * @return Ist dieses Level abgeschlossen, so wird ein true returnt
     */
    public boolean fetchThisLevelCompleted(int groupPosition, int childPosition) {

        boolean databaseThisLevelResult = false;
        uds.open();
        databaseThisLevelResult = uds.getCompletedOfLevel(groupPosition, childPosition);
        uds.close();

        return databaseThisLevelResult;
    }

    /**
     * Methode setzt ein checkmark neben das Level, das abgeschlossen wurde
     * @param v Die CheckTextView aus ThemenAdapter
     * @param parent Gruppenposition des Levels
     * @param child Kindposition des Levels
     */
    public void applyCheckMark(CheckedTextView v, int parent, int child, String childTitle) {
        ThemenAdapter adp = new ThemenAdapter(null, null, null);
        parent = adp.sortParents(parent);

        boolean levelCompleted;
        boolean levelUnlocked;

        //System.out.println(childTitle);

        levelCompleted = fetchThisLevelCompleted(parent, child);
        levelUnlocked = fetchFormerLevelCompleted(parent, child);

        if(levelCompleted || (childTitle.contains("Einsteiger") && levelCompleted)) {
            v.setCheckMarkDrawable(R.drawable.lvl_completed);
        } else {
            if(levelUnlocked) {
                v.setCheckMarkDrawable(R.drawable.lvl_unlocked);  // Ändern in unlocked icon
            } else {
                v.setCheckMarkDrawable(R.drawable.lvl_locked);
            }
            if(childTitle.contains("Einsteiger")) {
                v.setCheckMarkDrawable(null);
            }
        }
    }

    /**
     * Gibt die Anzahl der Level eines Themas aus der Datenbank zurück
     * @param themeID Position des Themas
     * @return Anzahl der Level
     */
    public float getLevelCount(int themeID) {
        float count = 0f;
        uds.open();
        count = uds.getLevelCount(themeID);
        uds.close();
        return count;
    }

    /**
     * Methode errechnet den Fortschritt eines Themas anhand der abgeschlossenen Level und schreibt diesen in die Datenbank
     * @param themeID ID des Themas
     */
    public float calculateThemeProgress(int themeID) {
        int totalLevelsCompleted = 0;
        int progressOfTheme = 0;

        for(int i = 0; i < (getLevelCount(themeID)); i++) {       // DB Methode
            boolean levelCompleted = fetchThisLevelCompleted(themeID, i);
            if(levelCompleted) {
                totalLevelsCompleted += 1;
            }
        }

        float percentage = (totalLevelsCompleted / (getLevelCount(themeID)));     // DB Methode

        uds.open();
        progressOfTheme = Math.round(percentage * 100f);
        uds.updateProgressOfTheme(themeID, progressOfTheme);
        uds.close();

        return progressOfTheme;
    }

}
