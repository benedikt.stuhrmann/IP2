package de.benediktstuhrmann.drawingtests;

import android.graphics.Bitmap;

/**
 * Diese Klasse dient dazu, Objekte zu instantieren, welche Informationen (über den Abschluss)
 * EINER Aufgabe des Levels enthalten.
 *
 * @author Benedikt Stuhrmann
 */
public class LevelEvaluation {

    //Levelname
    private String levelName;
    //Aufgabenstellung
    private String task;
    //Erzielte Punktzahl
    private int score;
    //Anzahl der benötigten Versuche
    private int strikes;
    //Screenshot der eigenen Lösung
    private Bitmap screenshot;
    //Erfolgreicher Abschluss?
    private boolean completed;

    /**
     * Konstruktor, welcher ein neues LevelEvaluation Objekt erstellt.
     * @param lvlName   Der Name der Aufgabe
     */
    public LevelEvaluation(String lvlName)
    {
        this.levelName = lvlName;
    }

    //--------------------------------------------------------------//
    // Getter und Setter zum Speichern bzw. Lesen der Informationen //
    //--------------------------------------------------------------//

    public String getLevelName() {
        return levelName;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getStrikes() {
        return strikes;
    }

    public void setStrikes(int strikes) {
        this.strikes = strikes;
    }

    public Bitmap getScreenshot() {
        return screenshot;
    }

    public void setScreenshot(Bitmap screenshot) {
        this.screenshot = screenshot;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}//end class
