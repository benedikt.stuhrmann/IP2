package de.benediktstuhrmann.drawingtests;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Max on 07.11.2016.
 */

public class ThemenAdapter extends BaseExpandableListAdapter{

    private Context context;
    private HashMap<String, List<String>> themen;   // <Geraden, (Level 1, Level 2, ...)>
    private List<String> level_List;                // Liste der Strings Geraden, ... (also Keyset der Themen HashMap)
    private LevelDataProvider ldp = new LevelDataProvider();

    // Constructor
    public ThemenAdapter(Context context, HashMap<String, List<String>> themen, List<String> level_List) {
        this.context = context;
        this.themen = themen;
        this.level_List = level_List;
    }

    /**
     * Diese Methode gibt das momentane Child als Object zurück
     * @param parent Momentanes Elternelement
     * @param child Momentanes Kind des Elternelements
     * @return Child als Object
     */
    @Override
    public Object getChild(int parent, int child) {
        return themen.get(level_List.get(parent)).get(child);
    }

    /**
     * @param parent s.o.
     * @param child s.o.
     * @return Id des momentanen Kindes
     */
    @Override
    public long getChildId(int parent, int child) {
        return child;
    }

    /**
     * Diese Methode erstellt eine View für jedes Child
     * @param parent s.o.
     * @param child s.o.
     * @param isLastChild gibt zurück, ob letztes Child erreicht wurde
     * @param convertView momentane Ansicht des aktuellen Kindes/der Gruppe
     * @param parentView die View
     * @return View des Childs
     */
    @Override
    public View getChildView(int parent, int child, boolean isLastChild, View convertView, ViewGroup parentView) {
        parent = sortParents(parent);
        LevelDataProvider ldp = new LevelDataProvider();

        String child_title = (String) getChild(parent, child);

        if(convertView == null) {
            // creates a new convertView (present View for current Child)
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.lvl_select_child_layout, parentView, false);
        }

        Typeface typeFace = Typeface.createFromAsset(context.getAssets(),"fonts/pink.ttf");

        CheckedTextView child_textView = (CheckedTextView) convertView.findViewById(R.id.lvl_select_child);
        child_textView.setTypeface(typeFace);
        child_textView.setText(child_title);

        child_textView.setCheckMarkDrawable(R.drawable.lvl_locked);

        ldp.applyCheckMark(child_textView, parent, child, child_title);

        return convertView;
    }

    /**
     * Methode gibt Anzahl der Childs in einer Liste zurück
     * @param groupPosition Welche Liste
     * @return int
     */
    @Override
    public int getChildrenCount(int groupPosition) {
        return themen.get(level_List.get(groupPosition)).size();
    }

    /**
     * Methode gibt Titel der momentanen Liste zurück
     * @param groupPosition momentane Liste
     * @return Object
     */
    @Override
    public Object getGroup(int groupPosition) {
        return level_List.get(groupPosition);
    }

    /**
     * Methode gibt Anzahl der Level in der Liste zurück
     * @return int
     */
    @Override
    public int getGroupCount() {
        return level_List.size();
    }

    /**
     * Methode gibt ID the Gruppe zurück
     * @param groupPosition s.o.
     * @return long
     */
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    /**
     * Methode setzt den Fortschritt des Themas in der View, Fortschritt resultiert aus DB-Abfrage
     * @param groupPosition Themen ID
     * @return Fortschritt des Themas
     */
    public int getGroupProgress(int groupPosition) {
        groupPosition = sortParents(groupPosition);
        LevelSelectActivity lsa = new LevelSelectActivity();
        UserDataSource uds = new UserDataSource(lsa.ctx);

        int progress = 0;
        uds.open();
        if(groupPosition == 0) {
            progress = uds.getProgressOfTheme(0);
        } else if(groupPosition == 1) {
            progress = uds.getProgressOfTheme(1);
        } else if(groupPosition == 2) {
            progress = uds.getProgressOfTheme(2);
        } else if(groupPosition == 3) {
            progress = uds.getProgressOfTheme(3);
        }

        uds.close();
        return progress;

    }

    /**
     * Erstellt die Gruppenansicht
     * @param parent s.o.
     * @param isExpanded ob die Gruppe ausgeklappt ist
     * @param convertView s.o.
     * @param parentView s.o.
     * @return die Gruppenansicht
     */
    @Override
    public View getGroupView(int parent, boolean isExpanded, View convertView, ViewGroup parentView) {
        parent = sortParents(parent);

        String group_Title = (String) getGroup(parent);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.lvl_select_parent_layout, parentView, false);
        }

        Typeface typeFace = Typeface.createFromAsset(context.getAssets(),"fonts/pink.ttf");
        TextView parent_textView = (TextView) convertView.findViewById(R.id.lvl_select_parent);
        parent_textView.setTypeface(typeFace);
        parent_textView.setText(group_Title);

        ImageView parent_imageView = (ImageView) convertView.findViewById(R.id.Spickzettel);
        parent_imageView.setClickable(true);
        parent_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent swapToSpickZettel = new Intent(context, SpickzettelTest.class);
                context.startActivity(swapToSpickZettel);
            }
        });

        ldp.calculateThemeProgress(parent);         // Berechnet Fortschritt und schreibt ihn in die DB

        int group_Progress = getGroupProgress(parent);      // Holt sich aktuellen Fortschritt wieder aus DB
        ProgressBar parent_prgBar = (ProgressBar) convertView.findViewById(R.id.lvl_prg_parent);
        parent_prgBar.setProgress(group_Progress);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    /**
     * Methode macht Level auswählbar, abhängig von Unlockstatus
     * @param groupPosition s.o.
     * @param childPosition s.o.
     * @return boolean ob Level auswählbar ist
     */
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        LevelDataProvider ldp = new LevelDataProvider();
        String child_title = (String) getChild(groupPosition, childPosition);

        if(child_title.contains("Einsteiger")) {
            return true;
        } else if(ldp.fetchThisLevelCompleted(groupPosition, childPosition) == true){
            return true;
        } else if(ldp.fetchFormerLevelCompleted(groupPosition, childPosition) == true) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Funktion sortiert die Positionen der Themen neu (HashMap wird nach gewollter Reihenfolge neu aufgestellt)
     * @param position gibt die alte Position an
     * @return den Integer mit der neuen Position
     */
    public int sortParents(int position) {
        int newPosition;

        if(position == 0) {
            newPosition = 2;
        } else if(position == 2) {
            newPosition = 0;
        } else {
            newPosition = position;
        }

        return newPosition;
    }
}
