package de.benediktstuhrmann.drawingtests;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.List;

/**
 * Die Gesamtauswertung am Ende eines jeden Levels
 */
public class LevelEvaluationActivity extends Activity {

    //Alle einzelnen Aufgaben des gespielten Levels
    private List<LevelEvaluation> levels = DrawGraphActivity.levelEvaluations;
    //Datenbank
    private UserDataSource uds = new UserDataSource(this);
    //ThemenId
    private int themeId;
    //LevelId
    private int lvlPos;
    //Anzahl der Aufgaben im Level
    private int lvlCount;
    //ID des nächsten Levels
    private int nextLvlId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_evaluation);
        final Context context = this;

        //Aufgaben des Levels mit Informationen über deren Abschluss darstellen
        this.populateListView();

        this.themeId = getIntent().getIntExtra("ThemeId", -1);
        this.lvlPos = getIntent().getIntExtra("LevelPos", -1);

        //In Datenbank abspeichern, dass das Level abgeschlossen wurde, wenn Level bestanden
        if(getIntent().getBooleanExtra("Completed", false))
        {
            try {
                uds.open();
                uds.updateCompletedOfLevel(themeId, lvlPos, 1);
                this.lvlCount = uds.getLevelCount(themeId);
                uds.close();
            } catch (Exception e) {
                System.out.println("Error beim Updaten der DB");
            }
        }

        //Zurück-Button, welcher zurück zur Levelauswahl leitet
        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/pink.ttf");
        TextView evaluHeader = (TextView) findViewById(R.id.txtView_Evaluation);
        evaluHeader.setTypeface(typeFace);

        Button btn_back = (Button) findViewById(R.id.btn_back);
        btn_back.setTypeface(typeFace);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent swapToSelect = new Intent(context, LevelSelectActivity.class);
                startActivity(swapToSelect);
            }
        });

        //NächstesLevel-Button, welcher (sofern vorhanden) direkt zum nächsten Level leitet
        Button btn_next = (Button) findViewById(R.id.btn_nextLvl);
        btn_next.setTypeface(typeFace);
        this.nextLvlId = lvlPos+1;
        // wenn noch weitere Level existieren und Level erfolgreich abgeschlossen wurde, ermöglichen direkt das nächste level zu starten
        if(nextLvlId < this.lvlCount && getIntent().getBooleanExtra("Completed", false))
        {
            btn_next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle ids = new Bundle();
                    ids.putInt("Themen_ID", themeId);
                    ids.putInt("Level_ID", nextLvlId);

                    Intent swapToDrawGraph = new Intent(context, DrawGraphActivity.class);
                    swapToDrawGraph.putExtras(ids);
                    startActivity(swapToDrawGraph);
                }
            });
        }
        else
            btn_next.setVisibility(View.GONE);


    }//end onCreate()

    /**
     * Befüllt die Liste mit allen Aufgaben des Levels
     */
    private void populateListView()
    {
        ArrayAdapter<LevelEvaluation> adapter = new evaluListAdapter();
        ListView list = (ListView) findViewById(R.id.list_levelEvaluation);
        list.setAdapter(adapter);
    }//end populateListView()

    /**
     * ListAdapter, welcher benötigt wird, um die ListView mit den Aufgaben des Levels zu befüllen
     */
    private class evaluListAdapter extends ArrayAdapter<LevelEvaluation>
    {

        public evaluListAdapter()
        {
            super(LevelEvaluationActivity.this, R.layout.item_view, levels);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            View itemView = convertView;
            if(itemView == null)
                itemView = getLayoutInflater().inflate(R.layout.item_view, parent, false);

            //Eintrag finden
            LevelEvaluation currentLvl= levels.get(position);

            if(currentLvl != null) {
                //in View einfügen ----------------------------------------------------------//
                Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/pink.ttf");

                TextView txtViewLvl = (TextView) itemView.findViewById(R.id.txtView_level);
                txtViewLvl.setTypeface(typeFace);
                txtViewLvl.setText(currentLvl.getLevelName());

                TextView txtViewTask = (TextView) itemView.findViewById(R.id.txtView_function);
                txtViewTask.setTypeface(typeFace);
                txtViewTask.setText("Zeichne die Funktion f(x) = " + currentLvl.getTask());

                TextView txtViewScore = (TextView) itemView.findViewById(R.id.txtView_score);
                txtViewScore.setTypeface(typeFace);
                txtViewScore.setText("Punkte: " + currentLvl.getScore());

                RatingBar strikes = (RatingBar) itemView.findViewById(R.id.strikesInLvl);
                strikes.setRating(currentLvl.getStrikes());

                ImageView imgView = (ImageView) itemView.findViewById(R.id.imgView_lvlScreenshot);
                imgView.setImageBitmap(currentLvl.getScreenshot());

                //nicht bestandene Aufgaben des Levels rot einfärben
                if(!currentLvl.isCompleted())
                {
                    RelativeLayout gradeColor =  (RelativeLayout) itemView.findViewById(R.id.completedColor);
                    gradeColor.setBackgroundColor(Color.argb(120, 191, 10, 0));
                }
            }

            return itemView;
        }

    }//end class

}//end class
