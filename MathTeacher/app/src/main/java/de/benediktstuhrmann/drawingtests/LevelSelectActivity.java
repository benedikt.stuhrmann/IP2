package de.benediktstuhrmann.drawingtests;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LevelSelectActivity extends AppCompatActivity {

    ExpandableListView lvl_select;
    HashMap<String, List<String>> themen;
    List<String> level_List;
    ThemenAdapter adapter;
    LevelDataProvider ldp;
    Button mainMenu_btn;
    public static Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ctx = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_select);

        final Context context = this;

        ldp = new LevelDataProvider();

        lvl_select = (ExpandableListView) findViewById(R.id.lvl_select);
        mainMenu_btn = (Button) findViewById(R.id.mainMenu_Btn);

        Typeface typeFace=Typeface.createFromAsset(getAssets(),"fonts/pink.ttf");
        mainMenu_btn.setTypeface(typeFace);

        themen = ldp.getInfo();
        level_List = new ArrayList<String>(themen.keySet());
        adapter = new ThemenAdapter(context, themen, level_List);

        lvl_select.setAdapter(adapter);

        lvl_select.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            int lastExpandedPosition;

            // Wenn eine Gruppe expanded wird, werden andere zugeklappt
            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != lastExpandedPosition) {
                    lvl_select.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

        lvl_select.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
            }
        });

        // Übergibt ausgewähltes Level an DrawGraphAct.
        lvl_select.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                
                Bundle ids = new Bundle();
                ids.putInt("Themen_ID", groupPosition);
                ids.putInt("Level_ID", childPosition);

                Intent swapToDrawGraph = new Intent(context, DrawGraphActivity.class);
                swapToDrawGraph.putExtras(ids);
                startActivity(swapToDrawGraph);

                return false;
            }
        });

        // Button, um zum Hpt.Menu zurückzukommen
        mainMenu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent swapToMainMenu = new Intent(context, MainMenuActivity.class);
                startActivity(swapToMainMenu);
            }
        });
    }
}
