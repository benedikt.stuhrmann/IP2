package de.benediktstuhrmann.drawingtests;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by nnamf on 23.11.2016.
 */

// Klasse zum erstellen der Datenbank mit gewünschten Feldern und Spalten
public class MySQLiteHelper extends SQLiteOpenHelper {

    // Datenbanknamen vergeben
    private static final String DATABASE_NAME = "benutzerdaten.db";
    // Datenbankversion festlegen
    private static final int DATABASE_VERSION = 1;

    // String mit SQL Befehlen zum erstellen der Tabelle "Thema"
    private static final String TABLE_CREATE_THEME =
                "CREATE TABLE THEME ("
            +   "THEME_ID INTEGER PRIMARY KEY, "
            +  	"LABEL TEXT, "
            + 	"PROGRESS INTEGER )";

    // String mit SQL Befehlen zum erstellen der Tabelle "Level"
    private static final String TABLE_CREATE_LEVEL =
                "CREATE TABLE LEVEL ("
            +  	"NAME TEXT,"
            +  	"COMPLETED INTEGER,"
            +  	"THEME_ID INTEGER,"
            +  	"LVL_POSITION INTEGER,"
            + 	"PRIMARY KEY (THEME_ID, LVL_POSITION) )";

    // String mit SQL Befehlen zum erstellen der Tabelle "Aufgaben"
    private static final String TABLE_CREATE_TASK =
                "CREATE TABLE TASK ("
            +   "THEME_ID INTEGER, "
            +  	"LVL_POSITION INTEGER,"
            +   "TASK_ID INTEGER, "
            +  	"TASK TEXT,"
            +  	"FUNCTION TEXT,"
            +  	"INTERVAL_START INTEGER,"
            +  	"INTERVAL_END INTEGER,"
            +  	"ZOOM INTEGER,"
            +  	"FUNCT_START INTEGER,"
            +  	"FUNCT_END INTEGER ,"
            +   "PRIMARY KEY (THEME_ID, LVL_POSITION, TASK_ID) )";


    //Constructor der die Datenbank mit Angaben erstellt
    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /** onCreate zum ausführen der im String vorhandenen SQL Befehle
     *
     * @param database Datenbank
     */
    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(TABLE_CREATE_THEME);
        System.out.println("THEME erstellt");
        database.execSQL(TABLE_CREATE_LEVEL);
        System.out.println("LEVEL erstellt");
        database.execSQL(TABLE_CREATE_TASK);
        System.out.println("TASK erstellt");
    }

    /** Falls Datenbank upgegradet wird
     *
     * @param db            Datenbank
     * @param oldVersion    alte Version
     * @param newVersion    neue Version
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS SCANITEM"); // fetsgelegte Dialoge
        onCreate(db);
    }





    public void deleteAllTables(SQLiteDatabase db)
    {
        db.execSQL("DROP TABLE IF EXISTS TASK");
        db.execSQL("DROP TABLE IF EXISTS LEVEL");
        db.execSQL("DROP TABLE IF EXISTS THEME");
    }//end deleteAllTables()

}
