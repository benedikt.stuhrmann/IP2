package de.benediktstuhrmann.drawingtests;

/**
 * Created by nnamf on 23.11.2016.
 */

// Klasse zum zwischen Speichern der aktuellen Einträge
public class Entry {

    // Variablen der Relation Thema
    private long theme_id;
    private String label;
    private int progress;

    // Variablen der Relation Level
    private String name;
    private boolean completed;
    private int lvl_position;

    // Variablen der Relation Aufgabe
    private long task_id;
    private String task;
    private String function;
    private int intervall_start;
    private int intervall_end;
    private int zoom;
    private int funct_start;
    private int funct_end;


    // get / set -Methoden Thema
    public long getTheme_id() {
        return theme_id;
    }

    public void setTheme_id(long theme_id) {
        this.theme_id = theme_id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) { this.label = label; }

    public int getProgress() { return progress; }

    public void setProgress (int progress) {
        this.progress = progress;
    }


    // get / set -Methoden Level
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getCompleted() { return completed; }

    public void setCompleted (boolean completed) {
        this.completed = completed;
    }

    public int getLvl_position() { return lvl_position; }

    public void setLvl_position (int lvl_position) {
        this.lvl_position = lvl_position;
    }


    // get / set -Methoden Aufgabe
    public long getTask_id() {
        return task_id;
    }

    public void setTask_id(long task_id) {
        this.task_id = task_id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getFunction() { return function; }

    public void setFunction(String function) { this.function = function; }

    public int getIntervall_start() { return intervall_start; }

    public void setIntervall_start (int intervall_start) { this.intervall_start = intervall_start; }

    public int getIntervall_end() { return intervall_end; }

    public void setIntervall_end (int intervall_end) { this.intervall_end = intervall_end; }

    public int getZoom() { return zoom; }

    public void setZoom (int zoom) { this.zoom = zoom; }

    public int getFunct_start() { return funct_start; }

    public void setFunct_start (int funct_start) { this.funct_start = funct_start; }

    public int getFunct_end() { return funct_end; }

    public void setFunct_end (int funct_end) { this.funct_end = funct_end; }

}
