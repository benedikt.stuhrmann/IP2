package de.benediktstuhrmann.drawingtests;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.TextView;

/**
 * PopUp-Fenster, welches zur Darstellung des Aufgabentextes
 * der jeweiligen Aufgabe dient.
 */
public class TaskPopUpActivity extends Activity {

    //Layoutelemente
    TextView task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_pop_up);

        //Größe des PopUp-Fensters setzen
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int) (width*0.7), (int) (height*0.7));

        //Extras auslesen
        String theTask = getIntent().getStringExtra("Task");
        int iStart = getIntent().getIntExtra("Interval_Start", 0);
        int iEnd = getIntent().getIntExtra("Interval_End", 0);

        //Text anzeigen
        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/pink.ttf");
        task = (TextView) findViewById(R.id.task);
        task.setTypeface(typeFace);
        task.setText("Zeichne die Funktion " + theTask + " im Intervall [" + iStart + ";" + iEnd + "]");

    }//end onCreate()

}//end class
