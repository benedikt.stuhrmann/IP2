package de.benediktstuhrmann.drawingtests;

import android.graphics.Path;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Der FunctionMaster dient dazu, die im Level hinterlegten Funktionen zu Berechnen und in
 * einen Pfad umzuwandeln, welcher dann anschließend zur Überprüfung genutzt werden kann.
 *
 * @author Benedikt Stuhrmann
 */
public class FunctionMaster {

    //Referenz zum Touch Event View über welchen gemalt wird
    private TouchEventView tev;
    //Die zu berechnende Funktion, ausgedrückt in einem String
    private String function;
    //Der aus der Funktion berechnete Graph
    private Path path= new Path();
    //Kritische Punkte der berechneten Funktion
    private HashMap<String, ArrayList<float[]>> critPoints = new HashMap<>();
    //Letzen beiden Punkte
    private float[][] prev = new float[2][2];

    //Koordinatensystem Einstellungen
    private float fromX;
    private float toX;
    //Genauigkeit der Berechnung
    private float stepsize;

    //Vergößerungsfaktor für Darstellung
    private int zoom;

    /**
     * Konstruktor, welcher einen FunctionMaster erstellt
     * @param tev       TouchEventView für welchen die Funktion berechnet werden soll
     * @param zoom      Skalierungsfaktor der Funktion
     */
    public FunctionMaster(TouchEventView tev, int zoom)
    {
        this.tev = tev;
        this.zoom = zoom;

        this.prev[0][0] = this.prev[0][1] = this.prev[1][0] = this.prev[1][1] = 9999;
    }//end FunctionMaster()

    /**
     * Ändert das Koordinatensystem und definiert in welchem Bereich eine Funktion berechnet werden soll
     * @param fromX         x Startwert
     * @param toX           x Endwert
     * @param stepsize      Genauigkeit der Berechnung (Schrittweite)
     */
    public void setGrid(float fromX, float toX, float stepsize)
    {
        this.fromX = fromX;
        this.toX = toX;
        this.stepsize = stepsize;
    }//end setGrid()

    /**
     * Berechnet die angegebene Polynomfunktion
     * @param ax        ax^3
     * @param bx        bx^2
     * @param cx        cx
     * @param d         d
     */
    public Path poly(float ax, float bx, float cx, float d)
    {
        this.critPoints = new HashMap<>(); //Reset der HashMap
        this.function = "f(x) = "+ax+"x^3+"+bx+"x^2+"+cx+"x+"+d;
        System.out.println(function);

        float y = 0;
        for(float x=fromX; x <= toX; x+=stepsize)
        {
            y = this.calculateCoordPoly(x, ax, bx, cx, d);
            this.checkForCritPoint(x, y);
            float[] xy = this.convertToCoords(x, y);
            this.updatePath(x, y, this.convertToCoords(x, y));
        }
        return path;
    }//end poly()

    /**
     * Berechnet die angegebene Sinusfunktion
     * @param a     Streckung bzw. Stauchung in y-Richtung (Amplitude)
     * @param bx    Graphisch bedeutet dies eine Streckung bzw. Stauchung in x-Richtung (Frequenz)
     * @param c     Verschiebung auf der x-Achse (Phasenverschiebung)
     * @param d     Verschiebung parallel der y-Achse um d(Schwingung)
     */
    public Path sin(float a, float bx, float c, float d)
    {
        this.critPoints = new HashMap<>(); //Reset der HashMap
        this.function = "f(x) = "+a+"*sin("+bx+"x+"+c+")+"+d;
        System.out.println(function);

        float y = 0;
        for(float x=fromX; x <= toX; x+=stepsize)
        {
            y = this.calculateCoordSin(x, a, bx, c, d);
            this.checkForCritPoint(x, y);
            float[] xy = this.convertToCoords(x, y);
            this.updatePath(x, y, this.convertToCoords(x, y));
        }
        return path;
    }//end sin()

    /**
     * Berechnet eine Cosinusfunktion
     * @param a     Streckung bzw. Stauchung in y-Richtung (Amplitude)
     * @param b    Graphisch bedeutet dies eine Streckung bzw. Stauchung in x-Richtung (Frequenz)
     * @param c     Verschiebung auf der x-Achse (Phasenverschiebung)
     * @param d     Verschiebung parallel der y-Achse um d(Schwingung)
     */
    public Path cos(float a, float b, float c, float d)
    {
        this.critPoints = new HashMap<>(); //Reset der HashMap
        this.function = "f(x) = "+a+"*cos("+b+"(x-"+c+"))+"+d;
        System.out.println(function);

        float y = 0;
        for(float x=fromX; x <= toX; x+=stepsize)
        {
            y = this.calculateCoordCos(x, a, b, c, d);
            this.checkForCritPoint(x, y);
            //System.out.println("x: "+x+", y: "+y);
            this.updatePath(x, y, this.convertToCoords(x, y));
        }
        return path;
    }//end cos()

    /**
     * Berechnet die angegebene Tangensfunktion
     * @param a     Streckung bzw. Stauchung in y-Richtung (Amplitude)
     * @param bx    Graphisch bedeutet dies eine Streckung bzw. Stauchung in x-Richtung (Frequenz)
     * @param c     Verschiebung auf der x-Achse (Phasenverschiebung)
     * @param d     Verschiebung parallel der y-Achse um d(Schwingung)
     */
    public Path tan(float a, float bx, float c, float d)
    {
        this.critPoints = new HashMap<>(); //Reset der HashMap
        this.function = "f(x) = "+a+"*tan("+bx+"x+"+c+")+"+d;
        System.out.println(function);

        float y = 0;
        for(float x=fromX; x <= toX; x+=stepsize)
        {
            float sinPart = this.calculateCoordSin(x, a, bx, c, d);
            float cosPart = this.calculateCoordCos(x, a, bx, c, d);
            y = sinPart/cosPart;

            this.checkForCritPoint(x, y);
            float[] xy = this.convertToCoords(x, y);
            this.updatePath(x, y, this.convertToCoords(x, y));
        }
        return path;
    }//end tan()

    /**
     * Berechnet den y-Wert für das übergebene x, der angegebenen Polynomfunktion
     * @param x         x-Wert für den y berechnet werden soll
     * @param ax        ax^3
     * @param bx        bx^2
     * @param cx        cx
     * @param d         d
     * @return          y
     */
    private float calculateCoordPoly(float x, float ax, float bx, float cx, float d)
    {
        return (float) ((ax * Math.pow(x,3)) + (bx * Math.pow(x,2)) + (cx * x) + d);
    }//end calculateCoordPoly()

    /**
     * Berechnet den y-Wert für das übergebene x, der angegebenen Sinusfunktion
     * @param x     x-Wert für den y berechnet werden soll
     * @param a     Streckung bzw. Stauchung in y-Richtung (Amplitude)
     * @param bx    Graphisch bedeutet dies eine Streckung bzw. Stauchung in x-Richtung (Frequenz)
     * @param c     Verschiebung auf der x-Achse (Phasenverschiebung)
     * @param d     Verschiebung parallel der y-Achse um d(Schwingung)
     * @return      y
     */
    private float calculateCoordSin(float x, float a, float bx, float c, float d)
    {
        return (float) (a * Math.sin((bx*x) + c) + d);
    }//end calculateCoordsSin()

    /**
     * Berechnet den y-Wert für das übergebene x, der angegebenen Cosinusfunktion
     * @param x     x-Wert für den y berechnet werden soll
     * @param a     Streckung bzw. Stauchung in y-Richtung (Amplitude)
     * @param b     Graphisch bedeutet dies eine Streckung bzw. Stauchung in x-Richtung (Frequenz)
     * @param c     Verschiebung auf der x-Achse (Phasenverschiebung)
     * @param d     Verschiebung parallel der y-Achse um d(Schwingung)
     * @return      y
     */
    private float calculateCoordCos(float x, float a, float b, float c, float d)
    {
        return (float) (a * Math.cos(b*(x-c)) + d);
    }//end calculateCoordsCos()

    /**
     * Fügt einen übergebenen Punkt dem Pfad hinzu, welcher basierend auf der Funktion erstellt wird.
     * @param x                 x-Koordinate des Punktes
     * @param y                 y-Koordinate des Punktes
     * @param xyConverted       x- und y-Koordinate in Bildschirmkoordinaten
     */
    private void updatePath(float x, float y, float[] xyConverted)
    {
        if (y <= tev.getHeight() / 2 || y >= -(tev.getHeight() / 2) || x <= tev.getWidth() / 2 || x >= -(tev.getWidth() / 2))
        {
            if (x == fromX)
                path.moveTo(xyConverted[0], xyConverted[1]);
            else
                path.lineTo(xyConverted[0], xyConverted[1]);
            tev.setSolutionPath(path);
        }
    }//end updatePath()

    /**
     * Wandelt Funktionswerte in Bildschirmkoordinaten um
     * @param x     x-Koordinate
     * @param y     y-Koordinate
     * @return      float-Array mit {x,y} umgewandelt in Bildschirmkoordinaten
     */
    private float[] convertToCoords(float x, float y)
    {
        float xZero = tev.getWidth()/2;
        float yZero = tev.getHeight()/2;

        return new float[] {(x*this.zoom+xZero), tev.getHeight()-(y*this.zoom+yZero)};
    }//end convertToCoords()

    /**
     * Überprüft den übergebenen Punkt darauf, ob er ein Punkt der Funktion ist, welcher stärker gewertet werden muss.
     * (Prüfung auf Nullstellen, y-Achsenabschnitt, Hochpunkte, Tiefpunkte)
     * @param x     x-Koordinate des Punktes
     * @param y     y-Koordinate des Punktes
     */
    private void checkForCritPoint(float x, float y)
    {
        //Punkt bestehend aus x und y coordinate
        float[] point = {x,y};

        //System.out.println("Aktueller Punkt:    {"+point[0]+", "+point[1]+"}");
        //System.out.println("Letzer Punkt:       {"+prev[0][0]+", "+prev[0][1]+"}");
        //System.out.println("Vorletzer Punkt:    {"+prev[1][0]+", "+prev[1][1]+"}");

        //Nullstelle
        if(y <= 0.1 && y >= -0.1)
        {
            //System.out.println("Nullpunkt gefunden! {" + x + ", " + y + "}");
            if(!critPoints.containsKey("Nullpunkt")) {
                ArrayList<float[]> aL = new ArrayList<>();
                aL.add(point);
                critPoints.put("Nullpunkt", aL);
                //System.out.println("Noch keine Nullpunkte vorhanden. Neuer Key erstellt!");
            }
            else{
                ArrayList<float[]> aL = critPoints.get("Nullpunkt");
                if(!this.checkIfAlreadyInList(x,y,aL))
                {
                    aL.add(point);
                    critPoints.remove("Nullpunkt");
                    critPoints.put("Nullpunkt", aL);
                }
            }
        }
        //y-Achsenabschnitt
        if(x <= 0.1 && x >= -0.1)
        {
            //System.out.println("y-Achsenabschnitt gefunden! {" + x + ", " + y + "}");
            if(!critPoints.containsKey("y-Achsenabschnitt")) {
                ArrayList<float[]> aL = new ArrayList<>();
                aL.add(point);
                critPoints.put("y-Achsenabschnitt", aL);
            }
            else{
                ArrayList<float[]> aL = critPoints.get("y-Achsenabschnitt");
                if(!this.checkIfAlreadyInList(x,y,aL))
                {
                    aL.add(point);
                    critPoints.remove("y-Achsenabschnitt");
                    critPoints.put("y-Achsenabschnitt", aL);
                }
            }
        }
        //Hochpunkt
        if(this.prev[0][1] > this.prev[1][1] && this.prev[0][1] > point[1])// && this.prev[0][1] != 9999 && this.prev[1][1] != 9999)
        {
            //System.out.println("-------------------------------------------------------------------------");
            //System.out.println("Hochpunkt gefunden! {" + this.prev[0][0] + ", " + this.prev[0][1] + "}");
            //System.out.println("-------------------------------------------------------------------------");

            if(!critPoints.containsKey("Hochpunkt")) {
                float xP = this.prev[0][0];
                float yP = this.prev[0][1];
                ArrayList<float[]> aL = new ArrayList<>();
                aL.add(new float[] {xP, yP});
                critPoints.put("Hochpunkt", aL);
                //System.out.println("Neuer Key erstellt!");
            }
            else{
                ArrayList<float[]> aL = critPoints.get("Hochpunkt");
                if(!this.checkIfAlreadyInList(this.prev[0][0], this.prev[0][1], aL))
                {
                    float xP = this.prev[0][0];
                    float yP = this.prev[0][1];

                    aL.add(new float[] {xP, yP});
                    critPoints.remove("Hochpunkt");
                    critPoints.put("Hochpunkt", aL);
                    //System.out.println("Zu vorhandenem Key hinzugefügt");
                }
            }
        }
        //Tiefpunkt
        if(this.prev[0][1] < this.prev[1][1] && this.prev[0][1] < point[1])
        {
            //System.out.println("Tiefpunkt gefunden! {" + x + ", " + y + "}");
            if(!critPoints.containsKey("Tiefpunkt")) {
                float xP = this.prev[0][0];
                float yP = this.prev[0][1];
                ArrayList<float[]> aL = new ArrayList<>();
                aL.add(new float[] {xP, yP});
                critPoints.put("Tiefpunkt", aL);
            }
            else{
                ArrayList<float[]> aL = critPoints.get("Tiefpunkt");
                if(!this.checkIfAlreadyInList(x,y,aL)) {
                    float xP = this.prev[0][0];
                    float yP = this.prev[0][1];
                    aL.add(new float[] {xP, yP});
                    critPoints.remove("Tiefpunkt");
                    critPoints.put("Tiefpunkt", aL);
                }
            }
        }

        //Update der jetzt letzten Punkte
        //"Letzter Punkt" wird zum "Vorletzen Punkt" und "jetziger Punkt" wird zum "letzen Punkt"
        this.prev[1][0] = this.prev[0][0];
        this.prev[1][1] = this.prev[0][1];
        this.prev[0][0] = point[0];
        this.prev[0][1] = point[1];

    }//end checkForCritPoints()

    /**
     * Überprüft ob ein übergebener Punkt bereits in der Liste vorhanden ist und doppelt eingefügt werden würde
     * @param x     x-Koordinate des Punktes
     * @param y     y-Koordinate des Punktes
     * @param aL    ArrayList welche float Arrays enthält, welche jeweils einen Punkt repräsentieren {x,y}
     * @return
     */
    private boolean checkIfAlreadyInList(float x, float y, ArrayList<float[]> aL)
    {
        for(float[] coord: aL)
        {
            boolean X = false;
            boolean Y = false;

            if((x >= (coord[0]-0.5f) && x <= (coord[0]+0.5f)))
            {
                //.out.println("x:           : " +x);
                //System.out.println("coord[0]     : " + coord[0]);
                //System.out.println("coord[0]-0.5f: " + (coord[0]-0.5f));
                //System.out.println("coord[0]+0.5f: " + (coord[0]+0.5f));
                //System.out.println("X-Wert bereits vorhanden");

                for(int i=0; i<aL.size(); i++) {
                    float[] point = aL.get(i);
                    //System.out.println("Liste    {" + point[0] + ", " + point[1] + "}");
                }

                X = true;
            }
            if((y >= (coord[1]-0.5f) && y <= (coord[1]+0.5f)))
            {
                //System.out.println("Y-Wert bereits vorhanden");
                Y = true;
            }
            if(X && Y)
            {
                //System.out.println("Koordinate bereits in Liste vorhanden");
                return true;
            }
        }

        return false;
    }//end checkIfAlreadyInList()

    /**
     * Liefert eine HashMap zurück, welche die Punkte der berechneten Funktion enthält, welche besonders gewertet werden sollen
     * @return      HashMap, String als Key (Bezeichnung des Punktes), ArrayList von float Arrays als Values.
     *              Float Array repräsentriert jeweils eine Koordinate {x,y}
     */
    public HashMap<String, ArrayList<float[]>> critPoints()
    {
        return this.critPoints;
    }//end critPoints()

}//end class
