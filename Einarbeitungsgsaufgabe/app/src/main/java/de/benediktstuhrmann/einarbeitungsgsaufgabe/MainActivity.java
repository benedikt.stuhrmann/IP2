package de.benediktstuhrmann.einarbeitungsgsaufgabe;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Button colorSwitch;
    FrameLayout colorBox;
    Random r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        colorSwitch = (Button) findViewById(R.id.btnColorSwitch);
        colorBox = (FrameLayout) findViewById(R.id.colorBox);
        r = new Random();

        //set a random color on start
        colorBox.setBackgroundColor(Color.rgb(r.nextInt(255), r.nextInt(255), r.nextInt(255)));

        colorSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorBox.setBackgroundColor(Color.rgb(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
            }
        });
    }
}
